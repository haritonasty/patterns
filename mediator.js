module.exports = function(include, puremvc) {

	puremvc.define(
		// CLASS INFO
		{
			name: "com.domain.mediator.ChainMediator",
			parent: puremvc.Mediator
		},

		// INSTANCE MEMBERS
		{
			listNotificationInterests: function() {
				return ["NEXT_IN_CHAIN","CHAIN_RESULT","CHAIN_DID_NOT_MATCH_REQUEST","CHAIN_CANCELED"]
			},
			handleNotification: function(note) {
				switch (note.getName()) {
					case "CHAIN_DID_NOT_MATCH_REQUEST":
						console.log("request did not match");
						break;
					case "CHAIN_CANCELED":
						console.log("action is canceled: "+(note.body||""));
						break;
					case "CHAIN_RESULT":
						console.log(note.body);
						break;
					case "NEXT_IN_CHAIN":
						this.next();
						break;
				}
			},
			onRegister: function() {
				this.chain = [];
				this.chainCommands = [];
				this.chainPointer = -1;
				this.data = null;
			},
			setData: function(data) {
				this.data = data;
			},
			getData: function(data) {
				return this.data;
			},
			setChain:function(chain) {
				this.chain = chain;
				for(var part in chain){
					this.chainCommands[part]=chain[part].COMMAND;
					this.facade.registerCommand(chain[part].COMMAND,chain[part]);
				}
			},
			getChain:function() {
				return this.chain;
			},
			next:function() {
				if(this.chainPointer < this.chain.length - 1) {
					this.chainPointer++;
					this.facade.sendNotification(this.chainCommands[this.chainPointer],this.data);
				} else {
					this.facade.sendNotification("CHAIN_DID_NOT_MATCH_REQUEST");
				}

			}
		},
		// STATIC MEMBERS
		{

			NAME: 'com.domain.mediator.ChainMediator'
		});
}