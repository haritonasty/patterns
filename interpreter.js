let assert = require('assert');
let readlineSync = require('readline-sync');

let variables = Object.create(null);
let queue = [];

function tokenize(code) {
	let results = [];
	let tokenRegExp = /\s*([A-Za-z]+|[0-9]+|\S)\s*/g;
	let m;
	while ((m = tokenRegExp.exec(code)) !== null)
		results.push(m[1]);
	return results;
}


function isNumber(token) {
	return token !== undefined && token.match(/^[0-9]+$/) !== null;
}

function isName(token) {
	return token !== undefined && token.match(/^[A-Za-z]+$/) !== null;
}

function parse(code) {


	let tokens = tokenize(code);
	let position = 0;

	function getPos() {
		return tokens[position];
	}

	function gotoNext(token) {
		assert.strictEqual(token, tokens[position]);
		position++;
	}


	function parsePrimaryExpr() {
		var t = getPos();

		if (isNumber(t)) {
			gotoNext(t);
			return {type: "number", value: t};
		} else if (isName(t)) {
			gotoNext(t);
			return {type: "name", id: t};
		} else if (t === "(") {
			gotoNext(t);
			var expr = parseExpr();
			if (getPos() !== ")")
				throw new SyntaxError("ожидается )");
			gotoNext(")");
			return expr;
		} else {

			throw new SyntaxError("Ожидается число, переменная или скобки");
		}
	}

	function parseStepExpr() {
		let expr = parsePrimaryExpr();
		let t = getPos();
		if (t === "=" && expr.type === "name") {
			gotoNext(t);
			let value = parsePrimaryExpr();
			if (value.type !== "number") throw new SyntaxError("ожидается число");
			variables[expr.id] = parseInt(value.value);
			expr = `${expr.id} = ${value.value}`;
			console.log(expr);
			t = getPos();
		}
		while (t === "^") {
			gotoNext(t);
			let rhs = parsePrimaryExpr();
			expr = {type: t, left: expr, right: rhs};
			t = getPos();
		}

		queue.push(expr);
		while (t === "#") {
			gotoNext(t);
			var rhs = parsePrimaryExpr();
			queue.push(rhs);
			expr = {type: t, left: expr, right: rhs};
			t = getPos();
		}
		if (queue.length > 2) {
			let second = queue.pop();
			let first = queue.pop();
			expr = {type: "#", left: first, right: second};
			queue = queue.reverse();
			queue.forEach(item => {
				"use strict";
				expr = {type: "#", left: item, right: expr};
			});

		}
		queue.length = null;
		return expr;
	}

	function parseMulExpr() {
		let expr = parseStepExpr();
		let t = getPos();

		while (t === "*") {
			gotoNext(t);
			let rhs = parseStepExpr();
			expr = {type: t, left: expr, right: rhs};
			t = getPos();
		}
		return expr;
	}

	function parseExpr() {
		let expr = parseMulExpr();
		let t = getPos();
		while (t === "+" || t === "-") {
			gotoNext(t);
			let rhs = parseMulExpr();
			expr = {type: t, left: expr, right: rhs};
			t = getPos();
		}
		return expr;
	}

	let result = parseExpr();

	if (position !== tokens.length)
		throw new SyntaxError("unexpected '" + getPos() + "'");

	return result;
}


// console.log(parse());
code = "(1+2)/3";

function getResult(code) {

	function getParam(obj) {
		switch (obj.type) {
			case "number":
				return parseInt(obj.value);
			case "name":
				return variables[obj.id] || 0;
			case "+":
				return getParam(obj.left) + getParam(obj.right);
			case "-":
				return getParam(obj.left) - getParam(obj.right);
			case "*":
				return getParam(obj.left) * getParam(obj.right);
			case "^":
				return Math.pow(getParam(obj.left), getParam(obj.right));
			case "#":
				return Math.pow(getParam(obj.left), getParam(obj.right));
		}
	}

	// console.log(parse(code));
	return getParam(parse(code));
}
while (true) {
	let input = readlineSync.question('> ').toString();
	try {
		if (input !== "exit") console.log(getResult(input)); else break;
	} catch (err) {
		console.log("Error: " + err.message);
	}


}
