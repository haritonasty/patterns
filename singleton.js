//		var obj = {
//			prop: 'value'
//		};
//
//		var obj2 = {
//			prop: 'value'
//		};

//		console.log(obj === obj2);
//		console.log(obj == obj2);


//		function Obj() {
//			if (typeof Obj.instance === "object") {
//				return Obj.instance;
//			}
//			Obj.instance = this;
//			return this;
//		};
//
//		var obj = new Obj();
//		var obj2 = new Obj();
//		console.log(obj === obj2);


function Obj() {
	//сохраненный экземпляр
	var instance = this;
	console.log('i\'ll appear once');
	//создать новый экземпляр
	this.a = 0;
	this.b = 1;
	//переопределить конструктор
	Obj = function () {
		console.log('hi there, I\'m new constructor');
		return instance;
	};
}
var obj = new Obj();
var obj2 = new Obj();
var obj3 = new Obj();
//
console.log(obj === obj2);

obj.c = 'shared';
//
console.log(obj);
//		console.log(obj2);