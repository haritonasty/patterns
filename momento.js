
let Memento = function (state) {

	let moment = state || 'undefined';

	this.getState = function () {
		return moment;
	};

};


let Caretaker = function () {

	let memento;

	this.getMemento = function () {
		return memento;
	};

	this.setMemento = function (_memento) {
		if (_memento instanceof Memento) {
			memento = _memento;
		}
	};

};



let Originator = function () {

	let temperature;
	let pressure;
	let volume;
	let result;

// сеттеры
	this.setTemperature = function (_state) {
		temperature = _state || "undefined";
	};
	this.setPressure = function (_state) {
		pressure = _state || "undefined";
	};
	this.setVolume = function (_state) {
		volume = _state || "undefined";
	};


// геттеры
	this.getTemperature = function () {
		return temperature;
	};
	this.getPressure = function () {
		return pressure;
	};
	this.getVolume = function () {
		return volume;
	};
	this.getResult = function () {
		return result;
	};


// сохранить состояние
	this.saveState = function () {
		return new Memento(state);
	};

// восстановить состояние
	this.restoreState = function (memento) {
		if (memento instanceof Memento) {
			state = memento.getState();
		}
	};

};





