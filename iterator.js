class Tree {

	constructor(size) {
		this.size = size;
		this.list = [];

		for (let i = 0; i < size; i++) {
			this.list.push([]);
		}
	}

	addVertex(u, v) {
		this.list[u].push(v);
	}

	dfs(s) {
		let vert = this.list;
		let visited = Array(vert).fill(false);
		let stack = [s];
		let v;

		visited[s] = true;

		return {
			[Symbol.iterator]() {
				return this;
			},
			next() {
				if (!stack.length) {
					return { done: true };
				}

				v = stack.pop();
				vert[v] = vert[v].reverse();
				vert[v].forEach(x => {
					if (!visited[x]) {
						stack.push(x);
						visited[x] = true;
					}
				});

				return { value: v }
			}
		};
	}

	bfs(s) {
		let vert = this.list;
		let visited = Array(vert).fill(false);
		let queue = [s];
		let v;

		visited[s] = true;

		return {
			[Symbol.iterator]() {
				return this;
			},
			next() {
				if (!queue.length) {
					return { done: true };
				}

				v = queue.shift();

				vert[v].forEach(x => {
					if (!visited[x]) {
						queue.push(x);
						visited[x] = true;
					}
				});

				return { value: v }
			}
		};
	}
}

let tree = new Tree(15);

tree.addVertex(1, 2);
tree.addVertex(1, 3);
tree.addVertex(1, 4);
tree.addVertex(2, 5);
tree.addVertex(2, 6);
tree.addVertex(4, 7);
tree.addVertex(4, 8);
tree.addVertex(7, 11);
tree.addVertex(7, 12);
tree.addVertex(5, 9);
tree.addVertex(5, 10);

console.log('bfs', [...tree.bfs(1)]);
console.log('dfs', [...tree.dfs(1)]);
